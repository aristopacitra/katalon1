Feature: Login

  Scenario Outline: Login with valid credentials
    Given User open FSM login page
    When User input <username> and <password>
    And Click login button
    Then User redirect to FSM homepage
    
   Examples:
    | username | password 				|
    | aristo 	 | iGDxf8hSRT4=     |