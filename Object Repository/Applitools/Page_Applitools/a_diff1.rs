<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_diff1</name>
   <tag></tag>
   <elementGuidId>c3222221-4107-40a9-ba21-5c330a740250</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'?diff1')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>a1e5942b-43fa-41d5-9efd-29c509747b42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>?diff1</value>
      <webElementGuid>4263a6d3-a06c-45f1-89f0-a2739141ff3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>?diff1</value>
      <webElementGuid>fc727196-0965-42a2-8fe6-8a499c79abbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;demo-page center&quot;]/div[@class=&quot;section&quot;]/p[2]/a[1]</value>
      <webElementGuid>71c541ae-74f7-4a4e-9b22-26128099f0ac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'?diff1')]</value>
      <webElementGuid>e4a4c609-7da3-43a5-b4a0-2b922cbd191e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='!'])[2]/following::a[1]</value>
      <webElementGuid>b7dc23df-5abe-45bd-9889-ba687f2a0c58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='d'])[2]/following::a[1]</value>
      <webElementGuid>52118430-3fc8-473d-8635-dead15078260</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='?diff2'])[1]/preceding::a[1]</value>
      <webElementGuid>8c284ab1-0cc1-4c1f-851b-4f528e3e19ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HH:MM:SS.sss'])[1]/preceding::a[2]</value>
      <webElementGuid>98f5ae92-0139-48c7-8ce9-93318fc23cdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='?diff1']/parent::*</value>
      <webElementGuid>457d252b-a93d-4435-84eb-a718d03ca8b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '?diff1')]</value>
      <webElementGuid>3240e29d-c79d-4cff-8a02-849c23b681b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>b1ff5aeb-3efb-4c2d-a401-ef84c19f66be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '?diff1' and (text() = '?diff1' or . = '?diff1')]</value>
      <webElementGuid>8505e560-6d0f-4df9-9abc-4e102f1ea594</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
