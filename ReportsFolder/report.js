$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/Login.feature");
formatter.feature({
  "name": "Login",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Login with valid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User open FSM login page",
  "keyword": "Given "
});
formatter.step({
  "name": "User input \u003cusername\u003e and \u003cpassword\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "Click login button",
  "keyword": "And "
});
formatter.step({
  "name": "User redirect to FSM homepage",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "aristo",
        "iGDxf8hSRT4\u003d"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Login with valid credentials",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User open FSM login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginStep.openFSMLoginPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input aristo and iGDxf8hSRT4\u003d",
  "keyword": "When "
});
formatter.match({
  "location": "LoginStep.enterCredentials(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click login button",
  "keyword": "And "
});
formatter.match({
  "location": "LoginStep.clickLoginButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User redirect to FSM homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStep.verifyHomepage()"
});
formatter.result({
  "status": "passed"
});
});